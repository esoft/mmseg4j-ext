package com.chenlb.mmseg4j.example;

import java.io.IOException;

import com.chenlb.mmseg4j.ComplexSeg;
import com.chenlb.mmseg4j.Seg;

public class Complex extends Simple
{
	protected Seg getSeg()
	{
		return new ComplexSeg(dic);
	}
	
	public static void main(String[] args) throws IOException
	{
		new Complex().run();
	}
}
